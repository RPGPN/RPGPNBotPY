import discord
from discord.ext import commands
import logging
from cogs import settings
import asyncio
import sqlitesys
import os

# MUST BE FIRST
settings.loadSettings()
dir_path = os.path.dirname(os.path.realpath(__file__))

bot = commands.Bot(command_prefix=commands.when_mentioned_or(settings.prefix), description="The Bot For RPGPN")


# Set up logs
def logEngine():
    logger = logging.getLogger('discord')
    logger.setLevel(logging.CRITICAL)
    handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)
    print("Logging To: Discord.log")


builtin = [
    "cogs.plugins",
    "cogs.stats",
    "cogs.admin",
    "cogs.owner",
    "cogs.fun",
    "cogs.scratch",
    "cogs.donate",
    "pluginmanager"

]
plugincogs = []


def plugin():
    pf = open(dir_path + "/plugins/plugins.rbot")
    pfarr = pf.readlines()
    for plugin in pfarr:
        plugincogs.append(plugin.strip())
    return


print('===COGS===')
for extension in builtin:
    try:
        bot.load_extension(extension)
        print('Loaded Cog: ' + extension)
    except Exception as e:
        print('Failed to load extension {}\n{}: {}'.format(extension, type(e).__name__, e))
print('===PLUGINS===')
plugin()
for extension in plugincogs:
    try:
        bot.load_extension(extension)
        print('Loaded Plugin-Cog: ' + extension)
    except Exception as e:
        print('Failed to load extension {}\n{}: {}'.format(extension, type(e).__name__, e))
print('\n')


@bot.event
async def on_ready():
    print("Logged in as " + str(bot.user.name) + " (" + str(bot.user.id) + ")")
    await bot.change_presence(game=discord.Game(name=settings.playing))
    # sqlitesys.sql_guilds_init_guild(bot.servers)


@bot.command(hidden=True)
async def joined(member : discord.Member):
    """Says when a member joined."""
    await bot.say('{0.name} joined in {0.joined_at}'.format(member))


@bot.command(hidden=True)
async def sqlconf():
    sqlitesys.sql_guilds_init()
    sqlitesys.sql_settings_init()

# Start Stuff

logEngine()
bot.run(settings.token)
