import sqlite3
import random
import os

global dir_path
global bot_path
def sql_settings_init():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    bot_path = dir_path[:-4]
    db = sqlite3.connect(dir_path + '/bot_data/botsettings')
    cursor = db.cursor()
    print('Opened DB')
    try:
        cursor.execute('''CREATE TABLE settings(id INTEGER PRIMARY KEY, name TEXT, value TEXT)''')
        cursor.execute('''CREATE TABLE members(id INTEGER PRIMARY KEY UNIQUE, name TEXT, mid TEXT)''')
        cursor.execute(
            '''CREATE TABLE guilds(id INTEGER, name TEXT, did TEXT PRIMARY KEY UNIQUE, region TEXT, icon TEXT)''')
        db.commit()
    except sqlite3.DatabaseError as de:
        print(de)
    db.close()


# def sql_guilds_init():
#     db = sqlite3.connect('server_data/servers')
#     print('Opened DB')
#     cursor = db.cursor()
#     cursor.execute(
#         '''CREATE TABLE guilds(id INTEGER PRIMARY KEY UNIQUE, name TEXT, did TEXT, region TEXT, icon TEXT)''')
#     db.close()


def sql_guilds_init_guild(servers):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    bot_path = dir_path[:-4]
    dba = sqlite3.connect(dir_path + '/bot_data/botsettings')
    dbb = sqlite3.connect(dir_path + '/server_data/servers')
    cursora = dba.cursor()
    cursorb = dbb.cursor()
    for server in servers:
        rid = random.randrange(1111111,99999999)
        Guild.id = server.id
        Guild.name = server.name
        Guild.icon = server.icon
        Guild.region = server.region
        try:
            cursorb.execute('''CREATE TABLE guilds(id INTEGER, name TEXT, did TEXT PRIMARY KEY UNIQUE, region TEXT, icon TEXT)''')
            try:
                cursora.execute('''INSERT INTO guilds(id,name,did,region,icon) VALUES (?, ?, ?, ?, ?)''', (
                    rid, server.name, server.id, str(server.region), server.icon
                ))
                dba.commit()

                try:
                    cursorb.execute('''INSERT INTO guilds(id,name,did,region,icon) VALUES (?, ?, ?, ?, ?)''', (
                        rid, server.name, server.id, str(server.region), server.icon
                    ))
                    dbb.commit()
                    print('boop')

                except sqlite3.OperationalError as error:
                    print(error)

            except sqlite3.OperationalError as error:
                print(error)

            print('fffff')

        except Exception as ex:
            print(ex)


    dba.close()
    dbb.close()
    return


class Guild:
    name: str = ''
    id: str = ''
    icon: str = ''
    region: str = ''