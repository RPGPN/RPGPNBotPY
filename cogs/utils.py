from discord.ext import commands
import discord.utils
import discord
from cogs import settings


def is_bot_admin(ctx):
    print(ctx.message.author.id)
    if ctx.message.author.id in settings.admins:
        return True
    else:
        return False


def is_owner(ctx):
    if ctx.message.author.id == settings.ownerid:
        return True
    else:
        return False
