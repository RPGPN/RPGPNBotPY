import discord
import os
import sys
import platform
from discord.ext import commands
from cogs import settings

class Stats:
    """Stats."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def botstats(self):
        """Get The Bot's Status"""
        embed = discord.Embed(title='Bot Status:\n', colour=discord.Color.lighter_grey())

        syst = str(platform.platform()) + ' on ' + str(platform.node())
        embed.add_field(name='System',value=syst, inline=True)

        hostname = str(platform.node())

        proc = str(platform.processor()) +  ' with ' + str(os.cpu_count()) + ' Cores'
        embed.add_field(name=hostname, value=proc, inline=False)

        python = str(sys.version_info.major) + '.' + str(sys.version_info.minor) + ' ; Discord Version: ' + str(discord.version_info.releaselevel)
        embed.add_field(name='Python', value=python, inline=False)
        embed.set_footer(text='Made with discord.py(Rapptz) By RPGPN', icon_url='http://i.imgur.com/5BFecvA.png')
        await self.bot.say(embed=embed)

    @commands.command(pass_context=True)
    async def stats(self,ctx, member: str = None):
        """Get A Member's Stats"""
        if member == None:
            member = ctx.message.author
            await self.bot.say(embed=self.getStats(ctx, member))
        else:
            await self.bot.say(embed=self.getStats(ctx, member))

    @commands.command()
    async def github(self):
        """Get The Bot's GitHub Link"""
        await self.bot.say('https://github.com/RPGPN/RPGPNBotPY/wiki')

    def getStats(self,ctx,member):
        embed = discord.Embed(colour=member.color)
        embed.set_author(name=member, icon_url=embed.Empty)
        if member.game:
            if member.game.name:
                # Playing a game!
                embed.add_field(name="Playing", value=str(member.game.name), inline=True)
        embed.set_footer(text='Made with discord.py(Rapptz) By RPGPN', icon_url='http://i.imgur.com/5BFecvA.png')
        return embed


def setup(bot):
    bot.add_cog(Stats(bot))
