import discord
import os
import sys
import platform
from discord.ext import commands
from cogs import settings

def setup(bot):
    bot.add_cog(Plugins(bot))

class Plugins:
    """Load/Unload/Reload/List Plugins Bot."""

    def __init__(self, bot):
        self.bot = bot
        global dir_path
        global bot_path_c
        global bot_path
        dir_path = os.path.dirname(os.path.realpath(__file__))

    @commands.command(pass_context=True)
    async def prl(self, ctx, *, plugin: str = None):
        """Reloads a plugin."""
        if ctx.message.author.id in settings.admins:
            if plugin is None:
                try:
                    self.bot.unload_extension(plugin)

                    self.bot.load_extension(plugin)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')

                    await self.bot.say('Please Specify A Plugin')

                else:
                    await self.bot.say('\N{OK HAND SIGN}')

                return

            elif plugin.lower().startswith('plugins.'):
                try:
                    self.bot.unload_extension(plugin)

                    self.bot.load_extension(plugin)

                    print('\nReloaded Plugin: ' + plugin)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')

                    await self.bot.say('{}: {}'.format(type(e).__name__, e))

                else:
                    await self.bot.say('\N{OK HAND SIGN}')

            elif not plugin.lower().startswith('plugins.'):
                plg = 'plugins.' + plugin

                try:
                    self.bot.unload_extension(plg)

                    self.bot.load_extension(plg)

                    print('\nReloaded Plugin: ' + plg)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')

                    await self.bot.say('{}: {}'.format(type(e).__name__, e))

                else:
                    await self.bot.say('\N{OK HAND SIGN}')
        else:
            await self.bot.say("You're Not An Admin!")

    @commands.command(pass_context=True)
    async def listpl(self,ctx):
        """List This Bot Instance's Plugins"""
        if ctx.message.author.id in settings.admins:
            await self.bot.say(self.pluginlist())
        else:
            await self.bot.say("You're Not An Admin!")

    @commands.command(pass_context=True)
    async def loadpl(self, ctx, *, plugin: str = None):
        """Loads a plugin."""
        if ctx.message.author.id in settings.admins:
            if plugin is None:
                try:
                    self.bot.load_extension(plugin)
                except Exception as e:
                    await self.bot.say('\N{PISTOL}')
                    await self.bot.say('Please Specify A Plugin')
                else:
                    await self.bot.say('\N{OK HAND SIGN}')
                return
            elif plugin.lower().startswith('plugins.'):
                try:
                    self.bot.load_extension(plugin)
                    print('\nLoaded Plugin: ' + plugin)
                except Exception as e:
                    await self.bot.say('\N{PISTOL}')
                    await self.bot.say('{}: {}'.format(type(e).__name__, e))
                else:
                    await self.bot.say('\N{OK HAND SIGN}')
            elif not plugin.lower().startswith('plugins.'):
                plg = 'plugins.' + plugin
                try:
                    self.bot.load_extension(plg)
                    print('\nLoaded Plugin: ' + plg)
                except Exception as e:
                    await self.bot.say('\N{PISTOL}')
                    await self.bot.say('{}: {}'.format(type(e).__name__, e))
                else:
                    await self.bot.say('\N{OK HAND SIGN}')
        else:
            await self.bot.say("You're Not An Admin!")

    @commands.command(pass_context=True)
    async def unloadpl(self, ctx, *, plugin: str = None):
        """Unloads a plugin."""
        if ctx.message.author.id in settings.admins:
            if plugin is None:
                try:
                    self.bot.unload_extension(plugin)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')
                    await self.bot.say('Please Specify A Plugin')
                else:
                    await self.bot.say('\N{OK HAND SIGN}')
                return
            elif plugin.lower().startswith('plugins.'):
                try:
                    self.bot.unload_extension(plugin)
                    print('\nUnloaded Plugin: ' + plugin)
                except Exception as e:
                    await self.bot.say('\N{PISTOL}')
                    await self.bot.say('{}: {}'.format(type(e).__name__, e))
                else:
                    await self.bot.say('\N{OK HAND SIGN}')
            elif not plugin.lower().startswith('plugins.'):
                plg = 'plugins.' + plugin
                try:
                    self.bot.unload_extension(plg)
                    print('\nUnloaded Plugin: ' + plg)
                except Exception as e:
                    await self.bot.say('\N{PISTOL}')
                    await self.bot.say('{}: {}'.format(type(e).__name__, e))
                else:
                    await self.bot.say('\N{OK HAND SIGN}')
        else:
            await self.bot.say("You're Not An Admin!")

    def pluginlist(self):
        pf = open(dir_path[:-4] + "plugins/plugins.rbot")
        plgmsg = '```\n'
        pfarr = pf.readlines()
        for plugin in pfarr:
            plgmsg += plugin + ' '
        plgmsg += '```'
        pf.close()
        return plgmsg

