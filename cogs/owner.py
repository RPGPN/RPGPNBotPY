import discord
import asyncio
from discord.ext import commands
from cogs import settings
from cogs import utils
import os

class Owner:
    """Owner-only commands."""
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def shutdown(self,ctx, force = None):
        if ctx.message.author.id == settings.ownerid:
            settings.settingstofile()
            quiet = False
            if force and force.lower() == 'force':
                quiet = True
            if not quiet:
                msg = 'Flushed settings to disk.\nShutting down...'
                await self.bot.say(msg)
            # Logout, stop the event loop, close the loop, quit
            for task in asyncio.Task.all_tasks():
                try:
                    task.cancel()
                except Exception:
                    continue
            try:
                await self.bot.logout()
                self.bot.loop.stop()
                self.bot.loop.close()
            except Exception:
                pass
            # Kill this process
            os._exit(3)
        elif ctx.message.author.id != settings.ownerid:
            await self.ctx.say("Oops! You're Not My **Real** Owner")

    @commands.command(pass_context=True)
    async def restart(self, ctx, force = None):
        if ctx.message.author.id == settings.ownerid:
            settings.settingstofile()
            quiet = False
            if force and force.lower() == 'force':
                quiet = True
            if not quiet:
                msg = 'Flushed settings to disk.\nRebooting...'
                await self.bot.say(msg)
            # Logout, stop the event loop, close the loop, quit
            for task in asyncio.Task.all_tasks():
                try:
                    task.cancel()
                except:
                    continue
            try:
                await self.bot.logout()
                self.bot.loop.stop()
                self.bot.loop.close()
            except:
                pass
            # Kill this process
            os._exit(2)
        elif ctx.message.author.id != settings.ownerid:
            await self.ctx.say("Oops! You're Not My **Real** Owner")

    @commands.command(pass_context=True)
    async def setgame(self,ctx, game: str):
        if ctx.message.author.id == settings.ownerid:
            print('Now Playing ' + game)

            await self.bot.change_presence(game=discord.Game(name=game))
            settings.playing = game
            await self.bot.say("Now Playing " + game)

        else:
            await self.bot.say("Oops! You're Not My **Real** Owner")

    @commands.command(pass_context=True)
    async def savesettings(self,ctx):
        if ctx.message.author.id == settings.ownerid:
            settings.settingstofile()
            await self.bot.say("Done!")

    @commands.command(pass_context=True)
    async def setprefix(self, ctx, sprefix: str = None):
        if ctx.message.author.id == settings.ownerid:
            if sprefix is None:
                await self.bot.say("Pop a Prefix After.")
            else:
                settings.prefix = sprefix
                await self.bot.say("Prefix is "+sprefix+"!")
                print(settings.prefix)

    # Cor, We're Done. Do Setup

def setup(bot):
    bot.add_cog(Owner(bot))