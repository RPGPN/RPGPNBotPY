import discord
from cogs import settings
from discord.ext import commands
import asyncio
from cogs import utils

class Admin:
    """Admin-only commands."""
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='crl', pass_context=True)
    async def crl(self, ctx, *, cogs: str = None):
        """Reloads a module."""
        if utils.is_bot_admin(ctx):
            if cogs is None:
                try:
                    self.bot.unload_extension(cogs)

                    self.bot.load_extension(cogs)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')

                    await self.bot.say('Please Specify A Module')

                else:
                    await self.bot.say('\N{OK HAND SIGN}')

                return

            elif cogs.lower().startswith('cogs.'):
                try:
                    self.bot.unload_extension(cogs)

                    self.bot.load_extension(cogs)

                    print('\nReloaded Cog: ' + cogs)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')

                    await self.bot.say('{}: {}'.format(type(e).__name__, e))

                else:
                    await self.bot.say('\N{OK HAND SIGN}')

            elif not cogs.lower().startswith('cogs.'):
                cog = 'cogs.' + cogs

                try:
                    self.bot.unload_extension(cog)

                    self.bot.load_extension(cog)

                    print('\nReloaded Cog: ' + cog)

                except Exception as e:
                    await self.bot.say('\N{PISTOL}')

                    await self.bot.say('{}: {}'.format(type(e).__name__, e))

                else:
                    await self.bot.say('\N{OK HAND SIGN}')
        else:
            await self.bot.say("You're Not An Admin!")


def setup(bot):
    bot.add_cog(Admin(bot))
