import discord
import os
import sys
import platform
from discord.ext import commands
from cogs import settings

class Fun:
    """Fun Stuff With The Bot."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def ping(self):
        """Play PingPong"""
        await self.bot.say('***Pong***')  # Meh

def setup(bot):
    bot.add_cog(Fun(bot))