import discord
import os
import sys
import platform
from discord.ext import commands
from cogs import settings

class Donate:
    """Donate To The Bot Or Server."""

    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context="True")
    async def donate(self):
        """Donations"""
        # await self.bot.say('```Donation Commands\n ;donate bot \n ;donate server```')

    @donate.command(name="botcreators")
    async def botcreators(self):
        """Get The Bot's Donation Link and Buy Ponkey A Coffee Or Something?"""
        await self.bot.say('Thanks, Kind Stranger! : https://patreon.com/rpgpn')

    @donate.command(name="server")
    async def server(self):
        """Get The Servers's Donation Link"""


def setup(bot):
    bot.add_cog(Donate(bot))
