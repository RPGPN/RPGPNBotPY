import logging
import datetime
import discord
import json
import copy
import os
import sqlite3
import sqlitesys

prefix = '$'
playing = ''
ownerid = ''
admins = []
token = ''
global dir_path
global bot_path_c
global bot_path

dir_path = os.path.dirname(os.path.realpath(__file__))
bot_path_c = dir_path
bot_path = bot_path_c[:-4]

# Token
if os.path.exists(bot_path + '/bot_data/token.rbot'):
    with open(bot_path + '/bot_data/token.rbot') as f:
        token = f.read()
else:
    print('Add A Token File')


def settingstofile():
    adtf = ''
    for id in admins:
        adtf += id
    dtw = prefix + '\n' + ownerid + '\n' + adtf + '\n' + playing
    f = open(bot_path + '/bot_data/settings.rbot', 'w')
    f.write(dtw)
    f.close()
    return
def loadSettings():

    global prefix
    global playing
    global admins
    global ownerid
    if os.path.exists(bot_path + 'bot_data/settings.rbot'):
        with open(bot_path + 'bot_data/settings.rbot') as f:
            settingsArr = f.readlines()
            prefix = str(settingsArr[0].strip())
            ownerid = str(settingsArr[1].strip())
            admins = str(settingsArr[2].strip())
            playing = str(settingsArr[3].strip())
    else:
        try:
            open(bot_path + '/bot_data/settings.rbot', 'x')
            # sqlitesys.sql_settings_init()
            # sqlitesys.sql_guilds_init()
        except FileExistsError:
            print('Something has gone horribly wrong?')
