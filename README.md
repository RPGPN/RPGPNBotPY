# RPGPNBOT

## Installation

[macOS](https://github.com/RPGPN/RPGPNBotPY/wiki/Setup#macos)

[Windows](https://github.com/RPGPN/RPGPNBotPY/wiki/Setup#windows)

[Linux](https://github.com/RPGPN/RPGPNBotPY/wiki/Setup#linux)
