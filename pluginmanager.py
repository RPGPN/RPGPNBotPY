import subprocess
import os
import sys
import discord
import asyncio
import wget
from discord.ext import commands
from cogs import settings
from cogs import utils


def setup(bot):
    bot.add_cog(plmgr(bot))

class plmgr:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    async def plmgr(self):
        """Plugin Manager System"""

    @plmgr.command(pass_context=True)
    async def get(self, ctx, plugin: str = None):
        """Get A Plugin"""
        if utils.is_bot_admin(ctx):
            if plugin is None:
                await self.bot.say("I Got **Nothin'**")
            else:
                plsfile = open("plugins.txt")
                plsarr = plsfile.readlines()
                plsfile.close()
                for line in plsarr:
                    ldata = line.split()
                    if ldata[2].lower() == plugin.lower():
                        url = ldata[0]
                        pl = subprocess.Popen(['curl', '-O', url])
                        pl.wait()
                        if os.name is 'nt':
                            try:
                                os.system('move ' + ldata[2] + '.py plugins/' + ldata[2] + '.py')
                                try:
                                    prbt = open('plugins/plugins.rbot', 'a')
                                    prbt.write('plugins.' + ldata[2] + ' \n')
                                    prbt.close()
                                    await self.bot.say("**Installed Plugin " + ldata[2] + ' !**')
                                except FileNotFoundError as e_b:
                                    await self.bot.say("File Error\n" + e_b)

                            except FileExistsError or FileNotFoundError as e_a:
                                await self.bot.say("File Error\n" + e_a)
                                return
                        else:
                            try:
                                os.system('mv ' + ldata[2] + '.py plugins/' + ldata[2] + '.py')
                                try:
                                    prbt = open('plugins/plugins.rbot', 'a')
                                    prbt.write('plugins.'+ldata[2])
                                    prbt.close()
                                    await self.bot.say("**Installed Plugin " + ldata[2] + ' !**')
                                except FileNotFoundError as e_b:
                                    await self.bot.say("File Error\n" + e_b)
                            except FileExistsError or FileNotFoundError as e_a:
                                await self.bot.say("File Error\n" + e_a)
                                return
        else:
            await self.bot.say("You're Not An Admin!")

    @plmgr.command(pass_context=True)
    async def supdate(self, ctx):
        if utils.is_bot_admin(ctx):
            url = "https://raw.githubusercontent.com/RPGPN/RPGPNBot_Plugins/master/plugins.txt"
            try:
                subprocess.Popen('curl -O '+url)
            except Exception as e:
                await self.bot.say("cURL Error\n" + e)
        else:
            await self.bot.say("You're Not An Admin!")

    @plmgr.command(pass_context=True)
    async def plupdate(self, ctx, all: bool = False, plugin: str = None):
        if utils.is_bot_admin(ctx):
            if all is False:
                if plugin is None:
                    await self.bot.say('Specify A Plugin Or True Instead Of False.')
                else:
                    await self.bot.say("Updating: " + plugin)
                    try:
                        self.bot.unload_extension(plugin)
                        print('\nUnloaded Plugin: ' + plugin)
                        plsfile = open("plugins.txt")
                        plsarr = plsfile.readlines()
                        plsfile.close()
                        for line in plsarr:
                            ldata = line.split()
                            if ldata[2].lower() == plugin.lower():
                                url = ldata[0]
                                pl = subprocess.Popen('curl -O ' + url)
                                pl.wait()
                                if os.name is 'nt':
                                    try:
                                        os.system('move /Y ' + ldata[2] + '.py plugins/' + ldata[2] + '.py')
                                        try:
                                            prbt = open('plugins/plugins.rbot', 'a')
                                            prbt.write('plugins.' + ldata[2] + ' \n')
                                            prbt.close()
                                            await self.bot.say("**Updated Plugin " + ldata[2] + ' !**')
                                        except FileNotFoundError as e_b:
                                            await self.bot.say("File Error\n" + e_b)

                                    except FileNotFoundError as e_a:
                                        await self.bot.say("File Error\n" + e_a)
                                        return
                                else:
                                    try:
                                        os.system('mv ' + ldata[2] + '.py plugins/' + ldata[2] + '.py')
                                        try:
                                            prbt = open('plugins/plugins.rbot', 'a')
                                            prbt.write('plugins.' + ldata[2])
                                            prbt.close()
                                            await self.bot.say("**Installed Plugin " + ldata[2] + ' !**')
                                        except FileNotFoundError as e_b:
                                            await self.bot.say("File Error\n" + e_b)
                                    except FileExistsError or FileNotFoundError as e_a:
                                        await self.bot.say("File Error\n" + e_a)
                                        return
                    except Exception as e:
                        await self.bot.say('{}: {}'.format(type(e).__name__, e))
                    else:
                        await self.bot.say('\N{OK HAND SIGN}')
            elif all is True:
                plfile = open("plugins.rbpl")
                plarr = plfile.readlines()
                plfile.close()
            else:
                await self.bot.say('```' + settings.prefix + 'plupdate [all]:{true/false} [plugin]```')
        else:
            await self.bot.say("You're Not An Admin!")